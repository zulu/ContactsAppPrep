﻿using ContactsAppPrep.Models;
using ContactsAppPrep.Services;
using GalaSoft.MvvmLight.Command;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using System;
using ContactsAppPrep.Classes;

namespace ContactsAppPrep.ViewModels
{
    public class EditContactViewModel : Contact, INotifyPropertyChanged
    {
        #region Attributes
        private DialogService dialogService;
        private ApiService apiService;
        private NavigationService navigationService;
        private bool isRunning;
        private bool isEnabled;
        private ImageSource imageSource;
        private MediaFile file;
        #endregion

        #region Properties
        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }

        public ImageSource ImageSource
        {
            set
            {
                if (imageSource != value)
                {
                    imageSource = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ImageSource"));
                }
            }
            get
            {
                return imageSource;
            }
        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructors
        public EditContactViewModel(Contact contact)
        {
            dialogService = new DialogService();
            apiService = new ApiService();
            navigationService = new NavigationService();

            ContactId = contact.ContactId;
            FirstName = contact.FirstName;
            LastName = contact.LastName;
            Image = contact.Image;
            EmailAddress = contact.EmailAddress;
            PhoneNumber = contact.PhoneNumber;

            IsEnabled = true;
        }
        #endregion

        #region Commands
        public ICommand DeleteContactCommand { get { return new RelayCommand(DeleteContact); } }

        private async void DeleteContact()
        {
            var answer = await dialogService.ShowConfirm("Confirm", "Are you sure to delete this record?");
            if (!answer)
            {
                return;
            }

            var contact = new Contact
            {
                EmailAddress = EmailAddress,
                FirstName = FirstName,
                LastName = LastName,
                PhoneNumber = PhoneNumber,
                Image = Image,
                ContactId = ContactId,
            };

            IsRunning = true;
            IsEnabled = false;
            var response = await apiService.Delete("http://contactsbackprep.azurewebsites.net", "/api", "/Contacts", contact);
            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            await navigationService.Back();
        }

        public ICommand SaveContactCommand { get { return new RelayCommand(SaveContact); } }

        private async void SaveContact()
        {
            if (string.IsNullOrEmpty(FirstName))
            {
                await dialogService.ShowMessage("Error", "You must enter a first name");
                return;
            }

            if (string.IsNullOrEmpty(LastName))
            {
                await dialogService.ShowMessage("Error", "You must enter a last name");
                return;
            }

            var imageArray = FilesHelper.ReadFully(file.GetStream());
            file.Dispose();

            var contact = new Contact
            {
                EmailAddress = EmailAddress,
                FirstName = FirstName,
                ImageArray = imageArray,
                LastName = LastName,
                PhoneNumber = PhoneNumber,
                Image = Image,
                ContactId = ContactId,
            };

            IsRunning = true;
            IsEnabled = false;
            var response = await apiService.Put("http://contactsbackprep.azurewebsites.net", "/api", "/Contacts", contact);
            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            await navigationService.Back();
        }

        public ICommand TakePictureCommand { get { return new RelayCommand(TakePicture); } }

        private async void TakePicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await dialogService.ShowMessage("No Camera", ":( No camera available.");
            }

            file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg",
                PhotoSize = PhotoSize.Small,
            });

            IsRunning = true;

            if (file != null)
            {
                ImageSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
            }

            IsRunning = false;
        }
        #endregion
    }
}
